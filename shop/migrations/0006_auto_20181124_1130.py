# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2018-11-24 11:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0005_auto_20181124_1129'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='color',
            field=models.CharField(max_length=200, verbose_name='Цвет'),
        ),
    ]
