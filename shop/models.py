from django.db import models
from django.core.urlresolvers import reverse

# Модель категории
class Collection(models.Model):
    name = models.CharField(max_length=250,db_index=True)
    slug = models.SlugField(max_length=250, db_index=True, unique=True)

    class Meta:
        ordering = ('name',)
        verbose_name= 'Коллекция'
        verbose_name_plural = 'Коллекции'
    def __str__(self):
        return self.name
    def get_absolute_url(self):
        return reverse('shop:ProductListByCollection', args=[self.slug])


class Category(models.Model):
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, db_index=True, unique=True)

    class Meta:
        ordering = ['name']
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name
    def get_absolute_url(self):
        return reverse('shop:ProductListByCategory', args=[self.slug])


# Модель продукта
class Product(models.Model):
    collection = models.ForeignKey(Collection, related_name='products', verbose_name="Коллекция")
    category = models.ForeignKey(Category, related_name='products', verbose_name="Категория")
    name = models.CharField(max_length=200, db_index=True, verbose_name="Название")
    slug = models.SlugField(max_length=200, db_index=True, unique=True)
    image = models.ImageField(upload_to='products/%Y/%m/%d/', blank=True, verbose_name="Изображение товара")
    image_1 = models.ImageField(upload_to='products/small/%Y/%m/%d/', blank=True, verbose_name="Маленькое изображение товара 1")
    image_2 = models.ImageField(upload_to='products/small/%Y/%m/%d/', blank=True, verbose_name="Маленькое изображение товара 2")
    image_3 = models.ImageField(upload_to='products/small/%Y/%m/%d/', blank=True, verbose_name="Маленькое изображение товара 3")
    description = models.TextField(blank=True, verbose_name="Описание")
    composition = models.TextField(blank=True, verbose_name="Состав")
    country = models.CharField(max_length=200, verbose_name="Страна производителея" )
    vars_clothes = models.CharField(max_length=200, verbose_name="Параметры модели" )
    color = models.CharField(max_length=200, verbose_name="Цвет" )
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name="Цена (рубли)")
    price_dollars = models.DecimalField(max_digits=10, decimal_places=2, verbose_name="Цена (доллары)")
    stock = models.PositiveIntegerField(verbose_name="На складе")
    available = models.BooleanField(default=True, verbose_name="Доступен")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['name']
        index_together = [
            ['id', 'slug']
        ]

    def __str__(self):
        return self.name
    def get_absolute_url(self):
        return reverse('shop:ProductDetail', args=[self.id, self.slug])
