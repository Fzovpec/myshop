from django.shortcuts import render, get_object_or_404
from .models import Category, Product, Collection
from cart.forms import CartAddProductForm
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth import authenticate, login
# Страница с товарами
def ProductList(request,category_slug=None,collection_slug=None):
    category = None
    categories = Category.objects.all()
    collection = None
    collections = Category.objects.all()
    products = Product.objects.filter(available=True)
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        products = products.filter(category=category)
    if collection_slug:
        collection = get_object_or_404(Collection, slug=collection_slug)
        products = products.filter(collection=collection)

    return render(request, 'shop/product/list.html', {
        'category': category,
        'categories': categories,
        'collection': collection,
        'collections': collections,
        'products': products
    })

# Страница товара
def ProductDetail(request, id, slug):
    product = get_object_or_404(Product, id=id, slug=slug, available=True)
    return render(request, 'shop/product/detail.html', {'product': product})
# Страница товара
def ProductDetail(request, id, slug):
    product = get_object_or_404(Product, id=id, slug=slug, available=True)
    cart_product_form = CartAddProductForm()
    return render_to_response('shop/product/detail.html',
                             {'product': product,
                              'cart_product_form': cart_product_form})
